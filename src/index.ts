import { ComponentResource, ComponentResourceOptions, Output } from "@pulumi/pulumi";
import { Role, Principal, PolicyDocument, RolePolicy, PolicyAttachment, GetPolicyResult, InstanceProfile, Ec2Principal, PolicyStatement } from "@pulumi/aws/iam";

export interface IamRoleOptions {
    attachedPolicyArns?: string[]
    description?: string
    inlinePolicies?: {[key: string]: PolicyDocument}
    name: string
    principals: Principal[]
    tags?: {[key: string]: any}
}

export class IamRole extends ComponentResource {
    public readonly role: Role

    public readonly policies: RolePolicy[] = []

    public readonly attachments: PolicyAttachment[] = []

    public readonly profile?: InstanceProfile

    public get arn(): Output<string> {
        return this.role.arn
    }

    public get id(): Output<string> {
        return this.role.id
    }

    public get name(): Output<string> {
        return this.role.name
    }

    constructor(name: string, config: IamRoleOptions, options?: ComponentResourceOptions) {
        super('sophosoft:aws:IamRole', name, {}, options)

        this.role = new Role(`${name}-role`, {
            name: config.name,
            description: config.description,
            assumeRolePolicy: {
                Version: "2012-10-17",
                Statement: config.principals.map((p: Principal): PolicyStatement => {
                    return { Effect: "Allow", Principal: p }
                })
            },
            tags: {
                ...config.tags,
                Name: config.name
            }
        }, { parent: this })

        if (config.inlinePolicies) {
            let index: number = 0
            for (let policyName in config.inlinePolicies) {
                this.policies.push(new RolePolicy(`${name}-policy-${++index}`, {
                    name: policyName,
                    policy: config.inlinePolicies[policyName],
                    role: this.role
                }, { parent: this.role }))
            }
        }

        if (config.attachedPolicyArns) {
            if (config.attachedPolicyArns.length > 10) {
                throw new Error(`Maximum attached policy limit exceeded.`)
            }
            let index: number = 0
            for (let policyArn of config.attachedPolicyArns) {
                this.attachments.push(new PolicyAttachment(`${name}-atch-${++index}`, {
                    policyArn,
                    roles: [this.role]
                }, { parent: this.role }))
            }
        }

        if (config.principals.includes(Ec2Principal)) {
            this.profile = new InstanceProfile(`${name}-profile`, {
                name: config.name,
                role: this.role
            }, { parent: this.role })
        }

        this.registerOutputs({
            role: this.role,
            policies: this.policies,
            attachments: this.attachments,
            profile: this.profile
        })
    }
}
