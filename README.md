# Pulumi IAM Role

Pulumi Component for AWS IAM Roles

## description

This component aggregates related IAM Role resources, such as an assume role policy, inline and attached policies, and instance profiles.  The inline policies are expressed as a map, with policy names as the keys.  Attached policies require valid ARNs.  If the defined principals include the `Ec2Principal`, an instance profile will automatically be generated.

> AWS limits roles to a maximum of 10 attached policies.

## install

```sh
npm i @sophosoft/pulumi-aws-iamrole
```

## usage

```ts
import { IamRole } from "@sophosoft/pulumi-aws-iamrole"
import { Ec2Principal, LambdaPrincipal, AmazonCognitoReadOnly, EcsPrincipal } from "@pulumi/aws/iam"

export const ec2Role = new IamRole('ec2', {
    name: 'ec2-test',
    principals: [Ec2Principal, EcsPrincipal],
    inlinePolicies: {
        test: {
            Version: "2012-10-17",
            Statement: [{
                Action: ["s3:GetObject"],
                Effect: "Allow"
            }]
        }
    }
})

export const lambdaRole = new IamRole('lambda', {
    name: 'lambda-test',
    principals: [LambdaPrincipal, { AWS: ['111122223333', '222233334444'] }],
    attachedPolicyArns: [AmazonCognitoReadOnly],
    tags: {
        Application: 'test-lambda'
    }
})
```

## constructor

```ts
new IamRole(name: string, config: IamRoleOptions, opts?: pulumi.ComponentResourceOptions)
```

### property **arn**

```ts
public arn: pulumi.Output<string>
```

### property **id**

```ts
public id: pulumi.Output<string>
```

### property **name**

```ts
public name: pulumi.Output<string>
```

### property **role**

```ts
public readonly role: aws.iam.Role
```

### property **policies**

```ts
public readonly policies: aws.iam.RolePolicy[]
```

### property **attachments**

```ts
public readonly attachments: aws.iam.PolicyAttachment[]
```

### property **profile**

```ts
public readonly profile?: aws.iam.InstanceProfile | undefined
```

## interface **IamRoleOptions**

| Property | Type | Description |
| -------- | ---- | ----------- |
| attachedPolicyArns | `string[] | undefined` | Optional list of policy arns to attach |
| description | `string | undefined` | Optional role description |
| inlinePolicies | `{[key: string]: PolicyDocument} | undefined` | Optional map of PolicyDocuments |
| name | `string` | A role name |
| principals | `Principal[]` | A list of assume-role principals |
| tags | `{[key: string]: any} | undefined` | Optional tags |
