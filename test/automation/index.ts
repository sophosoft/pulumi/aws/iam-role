import { IamRole } from "../../src"
import { runTests } from "../testHarness"
import { Ec2Principal, LambdaPrincipal, AmazonCognitoReadOnly, EcsPrincipal } from "@pulumi/aws/iam"

export const ec2Role = new IamRole('ec2', {
    name: 'ec2-test',
    principals: [Ec2Principal, EcsPrincipal],
    inlinePolicies: {
        test: {
            Version: "2012-10-17",
            Statement: [{
                Action: ["s3:GetObject"],
                Effect: "Allow"
            }]
        }
    }
})

export const lambdaRole = new IamRole('lambda', {
    name: 'lambda-test',
    principals: [LambdaPrincipal, { AWS: ['111122223333', '222233334444'] }],
    attachedPolicyArns: [AmazonCognitoReadOnly],
    tags: {
        Application: 'test-lambda'
    }
})

runTests(__dirname + '/..')
