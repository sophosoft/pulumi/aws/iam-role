import { expect } from "chai"
import { ec2Role, lambdaRole } from "./automation"
import { promise } from "./testHarness"

describe('IamRole', () => {
    describe('EC2 Role', () => {
        it('is named properly', async () => {
            const name = await promise(ec2Role.role.name)
            expect(name).to.equal('ec2-test')
        })

        it('has an EC2 principal', async () => {
            const policy = await promise(ec2Role.role.assumeRolePolicy)
            expect(policy).to.contain('ec2.amazonaws.com')
        })

        it('has an ECS principal', async () => {
            const policy = await promise(ec2Role.role.assumeRolePolicy)
            expect(policy).to.contain('ecs.amazonaws.com')
        })

        it('has an inline policy', async () => {
            const name = await promise(ec2Role.policies[0].name)
            expect(name).to.equal('test')
        })

        it('has a name tag', async () => {
            const tags = await promise(ec2Role.role.tags)
            expect(tags['Name']).to.equal('ec2-test')
        })

        it('has an instance profile', async () => {
            const profile = await promise(ec2Role.profile.name)
            expect(profile).to.equal('ec2-test')
        })

        it('has an arn property', async () => {
            const arn = await promise(ec2Role.arn)
            const roleArn = await promise(ec2Role.role.arn)
            expect(arn).to.equal(roleArn)
        })
    })

    describe('Lambda Role', () => {
        it('is named properly', async () => {
            const name = await promise(lambdaRole.role.name)
            expect(name).to.equal('lambda-test')
        })

        it('has a Lambda principal', async () => {
            const policy = await promise(lambdaRole.role.assumeRolePolicy)
            expect(policy).to.contain('lambda.amazonaws.com')
        })

        it('has account principals', async () => {
            const policy = await promise(lambdaRole.role.assumeRolePolicy)
            expect(policy).to.contain('111122223333')
            expect(policy).to.contain('222233334444')
        })

        it('has an attached policy', async () => {
            const arn = await promise(lambdaRole.attachments[0].policyArn)
            expect(arn).to.contain('AmazonCognitoReadOnly')
        })

        it('has extra tags', async () => {
            const tags = await promise(lambdaRole.role.tags)
            expect(tags['Application']).to.equal('test-lambda')
        })

        it('does not have an instance profile', async () => {
            expect(lambdaRole.profile).to.be.undefined
        })

        it('has an id property', async () => {
            const id = await promise(lambdaRole.id)
            const roleId = await promise(lambdaRole.role.id)
            expect(id).to.equal(roleId)
        })

        it('has a name property', async () => {
            const name = await promise(lambdaRole.name)
            const roleName = await promise(lambdaRole.role.name)
            expect(name).to.equal(roleName)
        })
    })
})
